# Coding Challenge

## Setup

* clone this repository
* `npm install`

## Run the server

* `node server.js`

## Run the tests

* run the server
* In another console, run `npm test`