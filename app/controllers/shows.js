
// Function that handle POST on /
exports.mainPostFn = function(req, res) {
	console.time('filterShows');

	// Filter the shows and simplify their JSON
	var simplifiedShows = [];
	if (req.body.payload) {
		simplifiedShows = filterShows(req.body.payload);
	} else {
		// The request doesn't contain a payload: error
		console.warn("Could not decode request: JSON parsing failed");
		res.status(400).send({
			error: "Could not decode request: JSON parsing failed"
		});
	}

	// Return the response containing the simplified JSON shows
	res.setHeader('Content-Type', 'application/json');
	res.end(JSON.stringify({ response: simplifiedShows }));

	console.timeEnd('filterShows');
};


// Filter the shows and return a table of simplified JSON
var filterShows = function(shows) {
	console.log('### in filterShows with', shows.length, 'shows');

	var simplifiedShows = [];
	for (var i = 0; i < shows.length; i++) {
		var show = shows[i];
		// Show DRM must be true and at least an episode
		if (show.drm === true && show.episodeCount > 0) {
			// Make sure that all needed fiels are present
			if (show.image.showImage && show.slug && show.title)
				simplifiedShows.push(simplifyShowJSON(show));
		}
	}

	return simplifiedShows;
};

// Unused function: filterShows is faster for at least 200 shows
var forEach = require('async-foreach').forEach;
var filterShowsAsync = function(shows) {
	console.log('### in filterShowsAsync with', shows.length, 'shows');
	console.time('filterShowsAsync');

	var simplifiedShows = [];
	forEach(shows, function(show, index, arr) {
		// Iteration become asynchronous
		var done = this.async();

		if (show.drm === true && show.episodeCount > 0) {
			simplifiedShows.push(simplifyShowJSON(show));
		}

		done();
	});

	console.timeEnd('filterShowsAsync');

	return simplifiedShows;
};

// return only the image, slug and title of the show
var simplifyShowJSON = function(show) {
	console.log('### in simplifyShowJSON for', show.title);

	return { 
		image: show.image.showImage,
		slug: show.slug,
		title: show.title
	};
};

