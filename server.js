var express = require('express');
var bodyParser = require('body-parser');

// Create an express app
var app = express();

// Parse application/x-www-form-urlencoded with qs library and a limit of 50mb instead of 1mb
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
// Parse application/json with a limit of 50mb instead of 1mb
app.use(bodyParser.json({limit: '50mb'}));

// Error handling
app.use(function (error, req, res, next) {
	// Invalid JSON: return a JSON response with HTTP status 400 Bad Request and an `error` key
	if (error instanceof SyntaxError) {
		console.warn("Could not decode request: JSON parsing failed");
		console.warn(error.stack);
		res.status(400).send({
			error: "Could not decode request: JSON parsing failed"
		});
	} else {
		next();
	}
});

// Get an instance of the express Router
var router = express.Router();              

// Application show controller
var shows = require('./app/controllers/shows');

// Default POST route on /
router.post('/', shows.mainPostFn);

// Register the router
app.use('/', router);

// Start the server
var port = process.env.PORT || 3000;
app.listen(port);
console.log('Server started on port ' + port);
