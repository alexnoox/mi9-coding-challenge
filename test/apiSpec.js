var assert = require('assert');
var request = require('supertest');

describe('API', function() {

	var url = 'http://alex-jarnoux-mi9.herokuapp.com';

	it('should send the sample request', function(done) {
		var params = require('./data/sample_request.json');
		var response = require('./data/sample_response.json');

		request(url)
		.post('/')
		.send(params)
		.expect('Content-Type', /json/)
		.expect(200, response)
		.end(function(err, res) {
			if (err) throw err;
			done();
		});
	});

	it('should send an empty request', function(done) {
		request(url)
		.post('/')
		.type('json')
		.send("")
		.expect('Content-Type', /json/)
		.expect(400)
		.end(function(err, res) {
			if (err) throw err;
			done();
		});
	});

	it('should send the broken request', function(done) {
		request(url)
		.post('/')
		.type('json')
		.send("{{} payl")
		.expect('Content-Type', /json/)
		.expect(400)
		.end(function(err, res) {
			if (err) throw err;
			done();
		});
	});

});
